var axios = require('axios');

var getExchangeRate = async (from, to) => {
    try{

        var response = await axios.get(`https://api.fixer.io/latest?base=${from}`);
        if(!response.data.rates[to]){
            throw new Error();
        }
        return response.data.rates[to];
    } catch (e) {
        throw new Error(`Unable to get exchange rate for ${from} to ${to}.`);
    }
};

var getCountries = async (currencyCode) => {
    try{

        var response = await axios.get(`https://restcountries.eu/rest/v2/currency/${currencyCode}`);
        return response.data.map((country) => country.name);
    } catch(e) {
        throw new Error(`Unable to get countries that use ${currencyCode}.`);
    }
};

var convertCurrency = async (from, to, amount) => {
    var countriesName = await getCountries(to);
    var currencyRate = await getExchangeRate(from, to);
    var exchangedAmount = amount * currencyRate;

    return `${amount} ${from} is worth ${exchangedAmount} ${to}. You can spend these in the following countries ${countriesName.join(', ')}.`;
};

convertCurrency('CAD', 'USD', 100).then((result) => {
    console.log(result);
}).catch((e) => {
    console.log(e.message);
});

/*
getExchangeRate ('CAD', 'EUR').then((rate) => {
    console.log(rate);
});

getCountries ('CAD').then((countryName) => {
    console.log(countryName);
});
*/